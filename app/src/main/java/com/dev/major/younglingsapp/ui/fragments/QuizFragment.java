package com.dev.major.younglingsapp.ui.fragments;


import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.NavController;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.dev.major.younglingsapp.Interfaces.OnDataAdded;
import com.dev.major.younglingsapp.R;
import com.dev.major.younglingsapp.adapters.OptionsAdapter;
import com.dev.major.younglingsapp.models.QuizModel;
import com.dev.major.younglingsapp.ui.viemmodels.QuizViewModel;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 */
public class QuizFragment extends Fragment implements OnDataAdded {
    private QuizViewModel quizViewModel;
    NavController navController;
    private RecyclerView recyclerView;
    private OptionsAdapter optionsAdapter;
    private LinearLayoutManager layoutManager;
    View view;
    private ArrayList<QuizModel> dataList = new ArrayList<>();

    public QuizFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_quiz, container, false);
        quizViewModel = ViewModelProviders.of(this).get(QuizViewModel.class);
        quizViewModel.init(getActivity());
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        recyclerView = view.findViewById(R.id.recyclerview_quiz_options);
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
        optionsAdapter = new OptionsAdapter(quizViewModel.getOptions().getValue());
        recyclerView.setAdapter(optionsAdapter);
    }

    @Override
    public void added() {
        optionsAdapter.notifyDataSetChanged();
    }
}
