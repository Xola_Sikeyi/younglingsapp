package com.dev.major.younglingsapp.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.dev.major.younglingsapp.R;
import com.dev.major.younglingsapp.models.QuizModel;

import java.util.ArrayList;

public class OptionsAdapter extends RecyclerView.Adapter<OptionsAdapter.ViewHolder>{

    private ArrayList<QuizModel> quizModels;

    public OptionsAdapter(ArrayList<QuizModel> quizModels) {
        this.quizModels = quizModels;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.button_list, parent,
                false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.itemView.setTag(quizModels.get(position));
        holder.text.setText(quizModels.get(position).getOption1());
    }

    @Override
    public int getItemCount() {
        return quizModels.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView text;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            text = itemView.findViewById(R.id.optionTextView);
        }
    }
}
