package com.dev.major.younglingsapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.nfc.Tag;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.dev.major.younglingsapp.models.Lists;
import com.dev.major.younglingsapp.models.QuizModel;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class QuizActivity extends AppCompatActivity implements View.OnClickListener{
    Button optionButton1, optionButton2, optionButton3, optionButton4;
    TextView questionText;
    private static final String TAG = "MyActivity";
    int index, score, totalQuestions;
    private FirebaseFirestore firebaseFirestore;
    public List<QuizModel> dataList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quiz);
        firebaseFirestore = FirebaseFirestore.getInstance();
        questionText = (TextView) findViewById(R.id.questionTextView);
        optionButton1 = (Button) findViewById(R.id.quizOption1);
        optionButton2 = (Button) findViewById(R.id.quizOption2);
        optionButton3 = (Button) findViewById(R.id.quizOption3);
        optionButton4 = (Button) findViewById(R.id.quizOption4);
        optionButton1.setOnClickListener((View.OnClickListener) this);
        optionButton2.setOnClickListener((View.OnClickListener) this);
        optionButton3.setOnClickListener((View.OnClickListener) this);
        Load();
    }

    private void Load() {
        firebaseFirestore.collection("Task").get().addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
            @Override
            public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                if (queryDocumentSnapshots != null) {
                    List<DocumentSnapshot> list = queryDocumentSnapshots.getDocuments();
                    for (DocumentSnapshot documentSnapshot : list) {
                        QuizModel quizModel = documentSnapshot.toObject(QuizModel.class);
                        Lists.modelList.add(quizModel);
                        totalQuestions = Lists.modelList.size();
                        Collections.shuffle(Lists.modelList);
                        populateData();
                    }
                }
            }
        });
    }

    private void populateData() {
        if (index < totalQuestions) {
            questionText.setText(Lists.modelList.get(index).getQuestion());
            optionButton1.setText(Lists.modelList.get(index).getOption1());
            optionButton2.setText(Lists.modelList.get(index).getOption2());
            optionButton3.setText(Lists.modelList.get(index).getOption3());
            optionButton4.setText(Lists.modelList.get(index).getOption4());
        }
    }

    @Override
    public void onClick(View view) {
        if (index < totalQuestions){
            Button buttonClicked = (Button) view;

            if (buttonClicked.getText().equals(Lists.modelList.get(index).getAnswer())){
                score += 1;
                //Toast.makeText(QuizActivity.this, "Correct", Toast.LENGTH_LONG).show();
                Handler handler = new Handler();
                Intent approvedIntent = new Intent(QuizActivity.this, ApprovedActivity.class);
                startActivity(approvedIntent);
                index++;
                populateData();
            }else {
                //Toast.makeText(QuizActivity.this, "Incorrect", Toast.LENGTH_LONG).show();
                Handler handler = new Handler();
                Intent disapprovedIntent = new Intent(QuizActivity.this, DisapprovedActivity.class);
                startActivity(disapprovedIntent);
                index++;
                populateData();

            }
        }
    }
}
