package com.dev.major.younglingsapp.ui.viemmodels;

import android.content.Context;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.dev.major.younglingsapp.Services.QuizService;
import com.dev.major.younglingsapp.models.QuizModel;

import java.util.ArrayList;

public class QuizViewModel extends ViewModel {
    MutableLiveData<ArrayList<QuizModel>> options;

    public void init(Context context){
        if (options != null){
            return;
        }
        options = QuizService.getInstance(context).getOptions();
    }

    public LiveData<ArrayList<QuizModel>> getOptions(){
        return options;
    }
}
