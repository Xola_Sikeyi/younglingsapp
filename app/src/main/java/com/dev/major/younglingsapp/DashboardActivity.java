package com.dev.major.younglingsapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class DashboardActivity extends AppCompatActivity {
    Button startQuiz, addQuestion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);
        startQuiz = (Button) findViewById(R.id.startQuizButton);
        addQuestion = (Button) findViewById(R.id.addQuestionButton);

        startQuiz.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent quizIntent = new Intent(DashboardActivity.this,QuizActivity.class);
                startActivity(quizIntent);
                finish();
            }
        });

        addQuestion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent addIntent = new Intent(DashboardActivity.this,AddActivity.class);
                startActivity(addIntent);
                finish();
            }
        });
    }
}
