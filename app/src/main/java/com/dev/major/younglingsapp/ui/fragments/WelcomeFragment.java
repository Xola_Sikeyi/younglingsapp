package com.dev.major.younglingsapp.ui.fragments;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.dev.major.younglingsapp.R;


public class WelcomeFragment extends Fragment {
    NavController navController;
    ImageView logo;

    public WelcomeFragment() {
        // Required empty public constructor
    }

    public View onCreateView(final LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                navController = Navigation.findNavController(container);
                navController.navigate(R.id.action_welcomeFragment_to_dashBoardFragment);
            }
        },4000);
        return inflater.inflate(R.layout.fragment_welcome, container, false);
    }

}
