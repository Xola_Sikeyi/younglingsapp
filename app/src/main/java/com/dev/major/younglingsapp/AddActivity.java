package com.dev.major.younglingsapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.HashMap;
import java.util.Map;

public class AddActivity extends AppCompatActivity {
    String option1, option2, option3, option4, answer, question;
    EditText et_option1, et_option2, et_option3, et_option4, et_answer, et_question;
    FirebaseFirestore firebaseFirestore;
    Button next, done;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add);
        firebaseFirestore = FirebaseFirestore.getInstance();
        et_answer = (EditText) findViewById(R.id.addAnswer);
        et_option1 = (EditText) findViewById(R.id.addOption1);
        et_option2 = (EditText) findViewById(R.id.addOption2);
        et_option3 = (EditText) findViewById(R.id.addOption3);
        et_option4 = (EditText) findViewById(R.id.addOption4);
        et_question = (EditText) findViewById(R.id.addQuestionEditText);
        next = (Button) findViewById(R.id.buttonNext);
        done = (Button) findViewById(R.id.buttonDone);

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                option1 = et_option1.getText().toString();
                option2 = et_option2.getText().toString();
                option3 = et_option3.getText().toString();
                option4 = et_option4.getText().toString();
                answer = et_answer.getText().toString();
                question = et_question.getText().toString();

                Map<String,Object> data = new HashMap<>();
                data.put("answer",answer);
                data.put("option1",option1);
                data.put("option2",option2);
                data.put("option3",option3);
                data.put("option4",option4);
                data.put("question",question);

                firebaseFirestore.collection("Task").add(data)
                        .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                            @Override
                            public void onSuccess(DocumentReference documentReference) {
                                Toast.makeText(AddActivity.this, "Added", Toast.LENGTH_LONG).show();
                                et_option1.setText("");
                                et_option2.setText("");
                                et_option3.setText("");
                                et_option4.setText("");
                                et_answer.setText("");
                                et_question.setText("");
                            }
                        });
            }
        });

        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent dashBoardIntent = new Intent(AddActivity.this, DashboardActivity.class);
                startActivity(dashBoardIntent);
                finish();
            }
        });
    }
}
