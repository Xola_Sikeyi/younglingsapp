package com.dev.major.younglingsapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

public class ApprovedActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_approved);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent quiz = new Intent(ApprovedActivity.this, QuizActivity.class);
                startActivity(quiz);
                finish();
            }
        }, 3000);
    }
}
