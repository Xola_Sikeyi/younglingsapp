package com.dev.major.younglingsapp.Services;

import android.content.Context;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;

import com.dev.major.younglingsapp.Interfaces.OnDataAdded;
import com.dev.major.younglingsapp.models.QuizModel;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;

public class QuizService {
    private static QuizService instance;
    private ArrayList<QuizModel> quizModels = new ArrayList<>();
    private FirebaseFirestore firebaseFirestore = FirebaseFirestore.getInstance();
    private static final String TAG = "QuizService";
    private static OnDataAdded onDataAdded;

    public static QuizService getInstance(Context context){
        if (instance == null){
            instance = new QuizService();
        }
        onDataAdded = (OnDataAdded) context;
        return instance;
    }

    public MutableLiveData<ArrayList<QuizModel>> getOptions(){
        loadData();
        MutableLiveData<ArrayList<QuizModel>> option = new MutableLiveData<>();
        option.setValue(quizModels);
        return option;
    }

    private void loadData() {
        firebaseFirestore.collection("Task").get().addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
            @Override
            public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                if (!queryDocumentSnapshots.isEmpty()){
                    List<DocumentSnapshot> list = queryDocumentSnapshots.getDocuments();
                    for (DocumentSnapshot documentSnapshot : list){
                        quizModels.add(documentSnapshot.toObject(QuizModel.class));
                    }
                    Log.e(TAG, "onSuccess: added");
                    onDataAdded.added();
                }
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.e(TAG, "onFailure", e);
            }
        });
    }
}
